from conans import ConanFile, CMake, tools, AutoToolsBuildEnvironment
from conans.tools import os_info, SystemPackageTool
import shutil
import os


class asn1(ConanFile):
    """
    Files are already compiled because they were patched to build on Windows. 
    """
    name = "asn1"
    version = "0.9.21"
    _sha256 = 'c68b7d34b572b93018625da890ceaac44bceb6528e03f231a5a72c4b707d0943'
    #_shared_lib_version = 10

    _source_dir = "asn1-%s" % version
    url = "https://github.com/Vladyslavru/conan_pack_asn1.git"
    requires = "zlib/1.2.11@lasote/stable"
    #settings = "os", "compiler", "build_type", "arch"
    #exports = "change_dylib_names.sh"
    #options = {"shared": [True, False], "fPIC": [True, False]}
    #default_options = "shared=False", "fPIC=False"
    #generators = "cmake"

    def config_options(self):
        self.options["zlib"].shared = self.options.shared
        # if self.settings.compiler == 'gcc' and float(self.settings.compiler.version.value) >= 5.1:
        #     if self.settings.compiler.libcxx != 'libstdc++11':
        #         raise ConanException("You must use the setting compiler.libcxx=libstdc++11")

    def source(self):
        download_filename = "conan_pack_asn1-master.zip"
        tools.download('https://github.com/Vladyslavru/conan_pack_asn1/archive/master.zip')
        tools.check_sha256(download_filename, self._sha256)
        tools.unzip(download_filename)
        os.unlink(download_filename)

